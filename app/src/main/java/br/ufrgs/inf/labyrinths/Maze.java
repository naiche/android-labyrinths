package br.ufrgs.inf.labyrinths;

import android.util.Log;

import java.util.Random;

/**
 * Created by arthursantos on 10/20/16.
 */

public class Maze {
    int rows;
    int cols;
    int act_rows;
    int act_cols;

    int entrance_row;
    int exit_row;

    char[][] board;
    char[][] correct_path;

    static final char VWALL = '|';
    static final char HWALL = '-';

    static final char MAZE_PATH = ' ';
    static final char MAZE_MARK = '.';

    public Maze(int row, int col)
    {

        //initialize instance variables
        rows = row*2+1;
        cols = col*2+1;
        act_rows = row;
        act_cols = col;
        board = new char[rows][cols];
        correct_path = new char[rows][cols];

        //set the maze to empty
        for(int i=0; i<rows; i++){
            for(int j=0; j<cols; j++){
                board[i][j] = MAZE_PATH;
            }
        }

        //make the outter walls
        for(int i=0; i<rows; i++){
            board[i][0] = VWALL;
            board[i][cols-1] = VWALL;
        }

        for(int i=0; i<cols; i++){
            board[0][i] = HWALL;
            board[rows-1][i] = HWALL;
        }

        makeMaze();
    }

    //storefront method to make the maze
    public void makeMaze()
    {
        makeMaze(0,cols-1,0,rows-1);
        makeOpenings();


    }

    //behind the scences actual mazemaking
    private void makeMaze(int left, int right, int top, int bottom)
    {
        int width = right-left;
        int height = bottom-top;

        //makes sure there is still room to divide, then picks the best
        //direction to divide into
        if(width > 2 && height > 2){

            if(width > height)
                divideVertical(left, right, top, bottom);

            else if(height > width)
                divideHorizontal(left, right, top, bottom);

            else if(height == width){
                Random rand = new Random();
                boolean pickOne = rand.nextBoolean();

                if(pickOne)
                    divideVertical(left, right, top, bottom);
                else
                    divideHorizontal(left, right, top, bottom);
            }
        }else if(width > 2 && height <=2){
            divideVertical(left, right, top, bottom);
        }else if(width <=2 && height > 2){
            divideHorizontal(left, right, top, bottom);
        }
    }

    private void divideVertical(int left, int right, int top, int bottom)
    {
        Random rand = new Random();

        //find a random point to divide at
        //must be even to draw a wall there
        int divide =  left + 2 + rand.nextInt((right-left-1)/2)*2;

        //draw a line at the halfway point
        for(int i=top; i<bottom; i++){
            board[i][divide] = VWALL;
        }

        //get a random odd integer between top and bottom and clear it
        int clearSpace = top + rand.nextInt((bottom-top)/2) * 2 + 1;

        board[clearSpace][divide] = MAZE_PATH;

        makeMaze(left, divide, top, bottom);
        makeMaze(divide, right, top, bottom);
    }

    private void divideHorizontal(int left, int right, int top, int bottom)
    {
        Random rand = new Random();

        //find a random point to divide at
        //must be even to draw a wall there
        int divide =  top + 2 + rand.nextInt((bottom-top-1)/2)*2;
        if(divide%2 == 1)
            divide++;

        //draw a line at the halfway point
        for(int i=left; i<right; i++){
            board[divide][i] = HWALL;
        }

        //get a random odd integer between left and right and clear it
        int clearSpace = left + rand.nextInt((right-left)/2) * 2 + 1;

        board[divide][clearSpace] = MAZE_PATH;

        //recur for both parts of the newly split section
        makeMaze(left, right, top, divide);
        makeMaze(left, right, divide, bottom);
    }

    public void makeOpenings(){

        Random rand = new Random(); //two different random number generators
        Random rand2 = new Random();//just in case

        //a random location for the entrance and exit
        entrance_row = rand.nextInt(act_rows-1) * 2 +1;
        exit_row = rand2.nextInt(act_rows-1) * 2 +1;

        //clear the location
        board[entrance_row][0] = MAZE_PATH;
        board[exit_row][cols-1] = MAZE_PATH;

    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<rows; i++){
            for(int j=0; j<cols; j++){
                if (board[i][j]==MAZE_PATH) sb.append(" ");
                else if (board[i][j]==VWALL) sb.append("#");
                else if (board[i][j]==HWALL) sb.append("#");
                else if (board[i][j]==MAZE_MARK) sb.append("*");
                else sb.append(" "); // this last case is for \0
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public String pathToString(){
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<rows; i++){
            for(int j=0; j<cols; j++){
                if (correct_path[i][j]==MAZE_PATH) sb.append(" ");
                else if (correct_path[i][j]==VWALL) sb.append(" ");
                else if (correct_path[i][j]==HWALL) sb.append(" ");
                else if (correct_path[i][j]==MAZE_MARK) sb.append("*");
                else sb.append(" "); // this last case is for \0
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    static final int DIRECTION_RIGHT = 0;
    static final int DIRECTION_UP = 1;
    static final int DIRECTION_DOWN = 2;
    static final int DIRECTION_LEFT = 3;

    //Global variables to wall follower algorithm
    Integer actualX;
    Integer actualY;
    int actualDirection;
    public void solveWallFollower(){
        actualX = 0;
        actualY = entrance_row;
        actualDirection = DIRECTION_RIGHT;
        this.board[actualY][actualX] = MAZE_MARK;
        this.correct_path[actualY][actualX] = MAZE_MARK;

        while (!exitFound()){
            actualDirection = turnRight(actualDirection);
            while (!canMoveToDirection(actualDirection)){
                actualDirection = turnLeft(actualDirection);
            }
            moveToDirection(actualDirection);
        }
    }

    boolean[][] wasHere;
    public void solveRecursivelly(){
        wasHere = new boolean[rows][cols];

        for (int row = 0; row < board.length; row++)
            // Sets boolean Arrays to default values
            for (int col = 0; col < board[row].length; col++){
                try{
                    wasHere[row][col] = false;
                    correct_path[row][col] = MAZE_PATH;
                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }
        boolean b = recursiveSolve(0, entrance_row);
    }

    private boolean recursiveSolve(int x, int y) {
        if (x == cols - 1 && y == exit_row) {
            wasHere[y][x] = true;
            board[y][x] = MAZE_MARK;
            correct_path[y][x] = MAZE_MARK;
            return true; // If you reached the end
        }
        if (board[y][x] == HWALL || board[y][x] == VWALL || wasHere[y][x]) return false;
        // If you are on a wall or already were here
        wasHere[y][x] = true;
        if (x != 0) // Checks if not on left edge
            if (recursiveSolve(x-1, y)) { // Recalls method one to the left
                correct_path[y][x] = MAZE_MARK; // Sets that path value to true;
                return true;
            }
        if (x != cols - 1) // Checks if not on right edge
            if (recursiveSolve(x+1, y)) { // Recalls method one to the right
                correct_path[y][x] = MAZE_MARK;
                return true;
            }
        if (y != 0)  // Checks if not on top edge
            if (recursiveSolve(x, y-1)) { // Recalls method one up
                correct_path[y][x] = MAZE_MARK;
                return true;
            }
        if (y != rows- 1) // Checks if not on bottom edge
            if (recursiveSolve(x, y+1)) { // Recalls method one down
                correct_path[y][x] = MAZE_MARK;
                return true;
            }
        return false;
    }

    private int turnRight(int direction) {
        switch (direction){
            case DIRECTION_RIGHT:
                return DIRECTION_DOWN;
            case DIRECTION_DOWN:
                return DIRECTION_LEFT;
            case DIRECTION_LEFT:
                return DIRECTION_UP;
            case DIRECTION_UP:
                return DIRECTION_RIGHT;
            default:
                return DIRECTION_RIGHT;
        }
    }

    private int turnLeft(int direction) {
        switch (direction){
            case DIRECTION_RIGHT:
                return DIRECTION_UP;
            case DIRECTION_DOWN:
                return DIRECTION_RIGHT;
            case DIRECTION_LEFT:
                return DIRECTION_DOWN;
            case DIRECTION_UP:
                return DIRECTION_LEFT;
            default:
                return DIRECTION_RIGHT;
        }
    }

    private boolean exitFound() {
        return (actualX == cols - 1 && actualY == exit_row);
    }

    public boolean canMoveToDirection(int direction){
        int x = actualX;
        int y = actualY;

        switch (direction){
            case DIRECTION_RIGHT:
                x++;
                break;
            case DIRECTION_LEFT:
                x--;
                break;
            case DIRECTION_UP:
                y--;
                break;
            case DIRECTION_DOWN:
                y++;
                break;
            default:
                break;
        }

        return (x >= 0 && x< cols)&&
                (y >= 0 && y< rows)&&
                (this.board[y][x] == MAZE_PATH || this.board[y][x] == MAZE_MARK);
    }

    public void moveToDirection(int direction){
        int x = actualX;
        int y = actualY;

        switch (direction){
            case DIRECTION_RIGHT:
                actualX++;
                break;
            case DIRECTION_LEFT:
                actualX--;
                break;
            case DIRECTION_UP:
                actualY--;
                break;
            case DIRECTION_DOWN:
                actualY++;
                break;
            default:
                break;
        }

        if (this.correct_path[actualY][actualX] == MAZE_MARK) {
            this.correct_path[y][x] = MAZE_PATH;
        }

        this.correct_path[actualY][actualX] = MAZE_MARK;
    }

    /*
    public void solveMazeRouting(){
        Point src = new Point(0, entrance_row);
        Point dst = new Point(cols-1, exit_row);
        Point cur = null;

        // cur also indicates the coordinates of the current location
        int MD_best = MD(src, dst);// It stores the closest MD we ever had to dst
        // A productive path is the one that makes our MD to dst smaller
        while(!cur.equals(dst)){
            if(there exists a productive path)
                //Take the productive path;
            else{
                MD_best = MD(cur, dst);
                //Imagine a line between cur and dst;
                //Take the first path in the left/right of the line;// The left/right selection affects the following hand rule
                while(MD(cur, dst) != MD_best || there does not exist a productive path)
                //Follow the right-hand/left-hand rule;// The opposite of the selected side of the line
            }
        }
    }

    private int MD(Point p0, Point p1){
        return Math.abs(p1.x-p0.x) + Math.abs(p1.y-p0.y);
    }
    */

}
